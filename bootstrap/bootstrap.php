<?php

use App\App;

require_once(__DIR__ . '/../vendor/autoload.php');
foreach (glob(__DIR__ ."/../helpers/*.php") as $helper) {
    require_once($helper);
}

$dotenv = new \Dotenv\Dotenv(__DIR__. '/../');
$dotenv->load();

set_exception_handler([new \App\Exceptions\Handler(), 'handle']);

App::run();