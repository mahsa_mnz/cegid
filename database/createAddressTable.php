<?php

require __DIR__ . '/../bootstrap/bootstrap.php';

$statement = <<<EOS
    CREATE TABLE IF NOT EXISTS addresses (
        id INT NOT NULL AUTO_INCREMENT,
        country VARCHAR(100) NOT NULL,
        state VARCHAR(100) NOT NULL,
        city VARCHAR(100) NOT NULL,
        zip_code VARCHAR(100) NOT NULL,
        address TEXT NOT NULL,
        
        PRIMARY KEY (id)
    ) ENGINE=InnoDB;
EOS;

$dbConnection = \App\Library\Database\DatabaseConnector::getInstance()->getConnection();

if (mysqli_query($dbConnection, $statement)) {
    echo "Table created successfully";
} else {
    throw new \Exception(mysqli_error($dbConnection));
}
