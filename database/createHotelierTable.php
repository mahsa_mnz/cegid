<?php

require __DIR__ . '/../bootstrap/bootstrap.php';

$statement1 = <<<EOS
    CREATE TABLE IF NOT EXISTS hoteliers (
        id INT NOT NULL AUTO_INCREMENT,
        address_id INT DEFAULT NULL,
        name VARCHAR(100) NOT NULL,
        rating SMALLINT UNSIGNED NOT NULL,
        category VARCHAR(100) NOT NULL,
        image VARCHAR(100) DEFAULT NULL,
        reputation INT UNSIGNED NOT NULL,
        reputation_badge VARCHAR(100) NOT NULL,
        price INT UNSIGNED NOT NULL,
        availability INT UNSIGNED NOT NULL,
        PRIMARY KEY (id)
    ) ENGINE=InnoDB;
EOS;

$statement2 = <<<EOS
    ALTER TABLE hoteliers ADD CONSTRAINT FK_hoteliers_address
      FOREIGN KEY (address_id) REFERENCES addresses(id);
EOS;

$dbConnection = \App\Library\Database\DatabaseConnector::getInstance()->getConnection();

if (mysqli_query($dbConnection, $statement1)) {
    echo "Table created successfully\n";
} else {
    throw new \Exception(mysqli_error($dbConnection));
}
if (mysqli_query($dbConnection, $statement2)) {
    echo "Table altered successfully\n";
} else {
    throw new \Exception(mysqli_error($dbConnection));
}