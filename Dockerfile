FROM php:8.0.0rc1-fpm

# Install system dependencies
RUN apt-get update && apt-get install -y git && apt-get install -y zip unzip

# Install PHP extensions
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Set working directory
WORKDIR /var/www

ARG WWW_DATA_USER
ARG WWW_DATA_GROUP

RUN usermod -u ${WWW_DATA_USER} www-data && \
    groupmod -g ${WWW_DATA_GROUP} www-data \