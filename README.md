# Cegid



## Getting started

copy .env.example to .env
```
cp .env.example .env
```
## Run the docker

Run the docker file
```
docker-compose up -d
```

## Install all dependencies

Run this command to install all dependencies with composer
```
docker exec -it php-app composer install
```

## create databases
Run these commands to create tables
```
docker exec -it php-app php database/createAddressTable.php
docker exec -it php-app php database/createHotelierTable.php
```


## Run
- now test the APIs with the given postman collection in root directory
- base_url : localhost:8000
