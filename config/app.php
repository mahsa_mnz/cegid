<?php

return [
    'log_path' => __DIR__ . '/../logs',
    'host' => getenv('DB_HOST'),
    'port' => getenv('DB_PORT'),
    'db'   => getenv('DB_DATABASE'),
    'user' => getenv('DB_USERNAME'),
    'pass' => getenv('DB_PASSWORD'),
];