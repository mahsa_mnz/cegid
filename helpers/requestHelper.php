<?php

/**
 * This function check if the given statement wrapped by braces
 * @param $statement
 * @return bool
 */
function isStringWrappedWithBrace($statement): bool
{
    if (str_starts_with($statement, "{") && str_ends_with($statement, "}"))
        return true;
    return false;
}

/**
 * This function get the word from the given statement which is wrapped by braces
 * @param $string
 * @return string
 */
function getStringBetweenBrace($string): string
{
    $string = ' ' . $string;
    $ini = strpos($string, "{");
    if ($ini == 0) return '';
    $ini += strlen("{");
    $len = strpos($string, "}", $ini) - $ini;
    return substr($string, $ini, $len);
}