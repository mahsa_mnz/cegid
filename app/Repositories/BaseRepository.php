<?php

namespace App\Repositories;

use App\Library\Database\DatabaseConnector;
use App\Models\ModelInterface;
use mysqli;

class BaseRepository
{
    protected ModelInterface $model;
    protected mysqli $db;

    protected array|string $columns = '*';

    public function __construct()
    {
        /* Get an object instance of the database */
        $database_object = DatabaseConnector::getInstance();
        /* Get a connection from the database object */
        $this->db = $database_object->getConnection();
    }

    public function select(array $columns): static
    {
        $this->columns = implode(", ", $columns);
        return $this;
    }

    public function all($whereArray = []): array
    {
        $tableName = $this->model->getTableName();

        $result = array();

        $statement = "SELECT $this->columns FROM $tableName";
        if ($count = count($whereArray))
        {
            $whereClause = '';
            $num = 0;
            foreach ($whereArray as $clause)
            {
                $num += 1;
                $whereClause .= $clause;
                if ($num < $count)
                    $whereClause .= "AND ";
            }
            $statement .= " WHERE ".$whereClause;
        }

        $queryResult = mysqli_query($this->db, $statement);

        if ($queryResult && mysqli_num_rows($queryResult) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($queryResult)) {
                $result[] = $row;
            }
        }
        return $result;
    }

    public function allPaginatedData(int $pageNum = 1, int $pageLimit = 20, $whereArray = [])
    {
        $tableName = $this->model->getTableName();
        if($pageNum <= 0)
            $pageNum = 1;
        $pageOffset = $pageLimit * ($pageNum - 1);

        $result = array();

        $statement = "SELECT $this->columns FROM $tableName";

        if ($count = count($whereArray))
        {
            $whereClause = '';
            $num = 0;
            foreach ($whereArray as $clause)
            {
                $num += 1;
                $whereClause .= $clause;
                if ($num < $count)
                    $whereClause .= "AND ";
            }
            $statement .= " WHERE ".$whereClause;
        }

        $PaginateStatement = $statement . " LIMIT $pageLimit OFFSET $pageOffset";

        $queryResult = mysqli_query($this->db, $PaginateStatement);

        if($queryResult && mysqli_num_rows($queryResult) > 0){

            // fetching data
            while($row = mysqli_fetch_assoc($queryResult)){
                $result['data'][] = $row;
            }

            $result['total'] = $total = mysqli_num_rows(mysqli_query($this->db, $statement));
            $result['total_pages'] = $totalPages = ceil($total / $pageLimit);
            $result['next_page'] = $pageNum+1 > $totalPages ? $pageNum : $pageNum+1;
            $result['previous_page'] = $pageNum-1 == 0 ? 1 : $pageNum-1;
        }
        return $result;
    }


    public function findBy($column, $value): array
    {
        $tableName = $this->model->getTableName();

        $result = array();

        $statement = "SELECT $this->columns FROM $tableName WHERE $column = $value;";

        $queryResult = mysqli_query($this->db, $statement);

        if (mysqli_num_rows($queryResult) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($queryResult)) {
                $result[] = $row;
            }
        }
        return $result;
    }

    public function findOneBy($column, $value): ?array
    {
        $tableName = $this->model->getTableName();
        $idName = $this->model->getIdName();
        $result = null;

        $statement = "SELECT $this->columns FROM $tableName WHERE $column = $value ORDER BY $idName ASC LIMIT 1;";

        $queryResult = mysqli_query($this->db, $statement);

        if (mysqli_num_rows($queryResult) == 1) {
            // output data of each row
            $result = mysqli_fetch_assoc($queryResult);
        }
        return $result;
    }

    public function create(array $attributes): array|string|null
    {
        $tableName = $this->model->getTableName();
        $idName = $this->model->getIdName();
        $columnsString = $this->camelToUnderscore(implode(", ", array_keys($attributes)));
        $values = array_values($attributes);

        $statement = 'INSERT INTO '.$tableName.' ('.$columnsString.') VALUES ("' . implode('", "', $values) . '");';

        $queryResult = mysqli_query($this->db, $statement);
        if ($queryResult) {
            $last_id = mysqli_insert_id($this->db);
            return $this->findOneBy($idName, $last_id);
        } else {
            return "Error: " . $statement . " : " . mysqli_error($this->db);
        }
    }

    public function update(int $id, array $attributes)
    {
        $tableName = $this->model->getTableName();
        $idName = $this->model->getIdName();

        $setter = '';
        $num = 0;
        $count = count($attributes);
        foreach ($attributes as $column => $value)
        {
            $num += 1;
            $setter .= $column."='".$value."'";
            if ($num < $count)
                $setter .= ", ";
        }

        $statement = "UPDATE $tableName SET $setter WHERE $idName=$id;";

        $queryResult = mysqli_query($this->db, $statement);
        if ($queryResult) {
            return true;
        } else {
            return "Error: " . $statement . " : " . mysqli_error($this->db);
        }
    }

    public function delete(int $id): bool|string
    {
        $tableName = $this->model->getTableName();
        $idName = $this->model->getIdName();
        $statement = "DELETE FROM $tableName WHERE $idName=$id";

        if (mysqli_query($this->db, $statement)) {
            return true;
        } else {
            return "Error deleting record: " . mysqli_error($this->db);
        }
    }

    public function decrease($id, $columnName)
    {
        $tableName = $this->model->getTableName();
        $idName = $this->model->getIdName();
        $statements = [
            "BEGIN;",
            "SELECT `$columnName` FROM `$tableName` WHERE `$idName` = $id FOR UPDATE;",
            "UPDATE `$tableName` SET `$columnName` = `$columnName`-1 WHERE `$idName` = $id;",
            "COMMIT;"
        ];

        foreach ($statements as $statement)
        {
            if (!mysqli_query($this->db, $statement))
            return "Error decreasing record: " . mysqli_error($this->db);
        }

        return true;
    }




    protected function camelToUnderscore($string): string
    {
        return strtolower(preg_replace('/(?<=\d)(?=[A-Za-z])|(?<=[A-Za-z])(?=\d)|(?<=[a-z])(?=[A-Z])/', "_", $string));
    }
}