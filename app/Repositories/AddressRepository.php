<?php

namespace App\Repositories;

use App\Models\Address;

class AddressRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new Address();
    }
}