<?php

namespace App\Repositories;


use App\Models\Hotelier;

class HotelierRepository extends BaseRepository
{
    private AddressRepository $addressRepository;

    public function __construct()
    {
        parent::__construct();
        $this->model = new Hotelier();
        $this->addressRepository = new AddressRepository();
    }

    public function create(array $attributes): array|string|null
    {
        $tableName = $this->model->getTableName();
        $idName = $this->model->getIdName();

        $addressParams = $attributes['address'];
        $address = $this->addressRepository->create($addressParams);

        unset($attributes['address']);
        $attributes['address_id'] = $address["id"];
        $attributes['reputation_badge'] = $this->model->calculateReputationBadge($attributes["reputation"]);

        $columnsString = $this->camelToUnderscore(implode(", ", array_keys($attributes)));
        $values = array_values($attributes);

        $statement = 'INSERT INTO '.$tableName.' ('.$columnsString.') VALUES ("' . implode('", "', $values) . '");';

        $queryResult = mysqli_query($this->db, $statement);
        if ($queryResult) {
            $last_id = mysqli_insert_id($this->db);
            return $this->findOneBy($idName, $last_id);
        } else {
            return "Error: " . $statement . " : " . mysqli_error($this->db);
        }
    }

    public function update(int $id, array $attributes)
    {
        $tableName = $this->model->getTableName();
        $idName = $this->model->getIdName();

        if (array_key_exists('address', $attributes)){
            $addressParams = $attributes['address'];
            $hotelier = $this->findOneBy("id", $id);
            $this->addressRepository->update($hotelier['address_id'], $addressParams);
            unset($attributes['address']);
        }
        if (array_key_exists('reputation', $attributes)){
            $attributes['reputation_badge'] = $this->model->calculateReputationBadge($attributes["reputation"]);
        }

        $setter = '';
        $num = 0;
        $count = count($attributes);
        foreach ($attributes as $column => $value)
        {
            $num += 1;
            $setter .= $column."='".$value."'";
            if ($num < $count)
                $setter .= ", ";
        }

        $statement = "UPDATE $tableName SET $setter WHERE $idName=$id;";

        $queryResult = mysqli_query($this->db, $statement);
        if ($queryResult) {
            return true;
        } else {
            return "Error: " . $statement . " : " . mysqli_error($this->db);
        }
    }
}