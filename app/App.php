<?php

namespace App;


use App\Library\Log\Logger;

class App
{
    public static function run()
    {
        Logger::enableSystemLogs();
    }
}