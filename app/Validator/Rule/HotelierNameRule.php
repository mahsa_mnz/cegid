<?php

namespace App\Validator\Rule;

use Rakit\Validation\Rule;

class HotelierNameRule extends Rule
{
    protected $message = ":attribute :value contains invalid word";

    protected array $invalidParams = ["Free", "Offer", "Book", "Website"];

    public function check($value): bool
    {
        foreach ($this->invalidParams as $invalidParam)
        {
            if (str_contains($value, $invalidParam)) {
                return false;
            }
        }
        return true;
    }
}