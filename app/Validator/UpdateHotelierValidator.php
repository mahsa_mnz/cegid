<?php

namespace App\Validator;

use App\Exceptions\ValidationException;
use App\Validator\Rule\HotelierNameRule;

class UpdateHotelierValidator
{
    /**
     * @throws ValidationException
     */
    public static function validate($inputArray)
    {
        $validator = new \Rakit\Validation\Validator();
        $validator->addValidator('hotelier_name', new HotelierNameRule());

        // make it
        $validation = $validator->make($inputArray, [
            "name"=> "alpha_spaces|min:10|hotelier_name",
            "rating"=> "numeric|digits_between:0,5",
            "category"=> "in:hotel,alternative,hostel,lodge,resort,guest-house",
            "reputation"=> "numeric|digits_between:0,1000",
            "price"=> "numeric",
            "availability"=> "numeric",
            "image" => "url|nullable",

            'address' => 'array',
            'address.country' => 'alpha_spaces',
            'address.state' => 'alpha_spaces',
            'address.city' => 'alpha_spaces',
            'address.zip_code' => 'numeric|min:5',
            'address.address' => 'required'
        ]);

        // then validate
        $validation->validate();

        if ($validation->fails()) {
            // handling errors
            $errors = $validation->errors()->toArray();

            throw new ValidationException("", 0, null, $errors);
        }
    }
}