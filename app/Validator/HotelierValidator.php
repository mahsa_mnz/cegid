<?php

namespace App\Validator;

use App\Exceptions\ValidationException;
use App\Validator\Rule\HotelierNameRule;
use Rakit\Validation\RuleQuashException;

class HotelierValidator
{
    /**
     * @throws ValidationException
     * @throws RuleQuashException
     */
    public static function validate($inputArray)
    {
        $validator = new \Rakit\Validation\Validator();
        $validator->addValidator('hotelier_name', new HotelierNameRule());

        // make it
        $validation = $validator->make($inputArray, [
            "name"=> "required|alpha_spaces|min:10|hotelier_name",
            "rating"=> "required|numeric|digits_between:0,5",
            "category"=> "required|in:hotel,alternative,hostel,lodge,resort,guest-house",
            "reputation"=> "required|numeric|digits_between:0,1000",
            "price"=> "required|numeric",
            "availability"=> "required|numeric",
            "image" => "url|nullable",

            'address' => 'required|array',
            'address.country' => 'required|alpha_spaces',
            'address.state' => 'required|alpha_spaces',
            'address.city' => 'required|alpha_spaces',
            'address.zip_code' => 'required|numeric|min:5',
            'address.address' => 'required'
        ]);

        // then validate
        $validation->validate();

        if ($validation->fails()) {
            // handling errors
            $errors = $validation->errors()->toArray();

            throw new ValidationException("", 0, null, $errors);
        }
    }
}