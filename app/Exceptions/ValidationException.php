<?php

namespace App\Exceptions;

use Exception;

class ValidationException extends Exception
{
    private $options;
    protected $message = "Invalid request!";

    public function __construct($message, $code = 0, Exception $previous = null, ?array $options = null)
    {
        parent::__construct($this->message, $code, $previous);

        $this->options = $options;
    }

    public function GetOptions(): ?array
    {
        return $this->options;
    }
}