<?php
namespace App\Exceptions;

use App\Library\Route\Response;
use Exception;
use JetBrains\PhpStorm\NoReturn;

class Handler
{
    #[NoReturn] public function handle(Exception $exception)
    {
        if ($exception instanceof NotFoundException)
            echo (new Response())->status(Response::RESPONSE_NOT_FOUND)->toJSON([
                'message' => $exception->getMessage()
            ]);
        else if ($exception instanceof ValidationException)
            echo (new Response())->status(Response::RESPONSE_BAD_REQUEST)->toJSON([
                'message' => $exception->getMessage(),
                'errors' => $exception->GetOptions()
            ]);
        else
            echo (new Response())->status(Response::RESPONSE_SERVER_ERROR)->toJSON([
                'message' => $exception->getMessage()
            ]);
        die();
    }
}