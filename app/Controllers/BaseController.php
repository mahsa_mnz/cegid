<?php
namespace App\Controllers;



use App\Library\Route\Response;
use JetBrains\PhpStorm\NoReturn;

class BaseController
{
    #[NoReturn] protected function returnOkResponse($data = null, $message = null)
    {
        $response = new Response();
        echo $response->toJSON([
            'data' =>  $data,
            'message' => $message
        ]);
        exit();
    }
    #[NoReturn] protected function returnCreatedResponse($data = null, $message = null)
    {
        $response = new Response();
        echo $response->status(Response::RESPONSE_CREATED)->toJSON([
            'data' =>  $data,
            'message' => $message
        ]);
        exit();
    }

    #[NoReturn] protected function returnBadRequestResponse($data = null, $message = null)
    {
        $response = new Response();
        echo $response->status(Response::RESPONSE_BAD_REQUEST)->toJSON([
            'data' =>  $data,
            'message' => $message
        ]);
        exit();
    }
}