<?php
namespace App\Controllers;

use App\Library\Route\Request;

interface ResourceControllerInterface
{
    public function index(Request $request);
    public function show(Request $request);
    public function create(Request $request);
    public function update(Request $request);
    public function delete(Request $request);
}