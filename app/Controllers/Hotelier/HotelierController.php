<?php
namespace App\Controllers\Hotelier;

use App\Controllers\BaseController;
use App\Controllers\ResourceControllerInterface;
use App\Exceptions\NotFoundException;
use App\Exceptions\ValidationException;
use App\Library\Route\QueryParser;
use App\Library\Route\Request;
use App\Repositories\HotelierRepository;
use App\Validator\HotelierValidator;
use App\Validator\UpdateHotelierValidator;

class HotelierController extends BaseController implements ResourceControllerInterface
{
    private HotelierRepository $hotelierRepository;

    public function __construct()
    {
        $this->hotelierRepository = new HotelierRepository();
    }

    public function index(Request $request)
    {
        $pageNum = 1;
        $pageLimit = 20;

        $queryParams = $request->getQueryStringParams();
        if (array_key_exists("page", $queryParams))
        {
            $pageNum = $queryParams["page"];
            unset($queryParams["page"]);
        }
        if (array_key_exists("per_page", $queryParams))
        {
            $pageLimit = $queryParams["per_page"];
            unset($queryParams["per_page"]);
        }
        $whereArray = QueryParser::parseQueryParams($queryParams);
        $data = $this->hotelierRepository->allPaginatedData($pageNum, $pageLimit, $whereArray);
        $this->returnOkResponse($data);
    }

    /**
     * @throws NotFoundException
     */
    public function show(Request $request)
    {
        $hotelierId = $request->params['hotelier_id'] ?? null;
        if ($hotelierId == null)
            throw new NotFoundException();

        $data = $this->hotelierRepository->findOneBy('id', $hotelierId);
        if (!$data)
            throw new NotFoundException();
        $this->returnOkResponse($data);
    }

    /**
     * @throws ValidationException
     */
    public function create(Request $request)
    {
        $requestBody = $request->getBody();
        HotelierValidator::validate($requestBody);

        $data = $this->hotelierRepository->create($requestBody);
        $this->returnCreatedResponse($data);
    }

    /**
     * @throws NotFoundException
     * @throws ValidationException
     */
    public function update(Request $request)
    {
        $hotelierId = $request->params['hotelier_id'] ?? null;
        if ($hotelierId == null)
            throw new NotFoundException();
        $hotelier = $this->hotelierRepository->findOneBy('id', $hotelierId);
        if (!$hotelier)
            throw new NotFoundException();

        $requestBody = $request->getBody();
        UpdateHotelierValidator::validate($requestBody);

        $data = $this->hotelierRepository->update($hotelierId, $requestBody);
        $this->returnOkResponse($data);
    }

    /**
     * @throws NotFoundException
     */
    public function delete(Request $request)
    {
        $hotelierId = $request->params['hotelier_id'] ?? null;
        if ($hotelierId == null)
            throw new NotFoundException();

        $hotelier = $this->hotelierRepository->findOneBy('id', $hotelierId);
        if (!$hotelier)
            throw new NotFoundException();

        $data = $this->hotelierRepository->delete($hotelierId);
        $this->returnOkResponse($data);
    }

    /**
     * @throws NotFoundException
     */
    public function book(Request $request)
    {
        $hotelierId = $request->params['hotelier_id'] ?? null;
        if ($hotelierId == null)
            throw new NotFoundException();

        $hotelier = $this->hotelierRepository->findOneBy('id', $hotelierId);
        if (!$hotelier)
            throw new NotFoundException();

        //check if is an available room
        if($hotelier['availability'] <= 0)
            $this->returnBadRequestResponse(false, "There is no available room!");

        //Reduce availability
        $data = $this->hotelierRepository->decrease($hotelierId, "availability");

        if($data === true)
            $this->returnOkResponse($data);
        else
            var_dump($data);
            $this->returnBadRequestResponse(false, "Action couldn't be completed!");
    }
}