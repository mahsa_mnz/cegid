<?php

namespace App\Library\Database;

use App\Library\Config\Config;
use mysqli;

class DatabaseConnector
{
    private ?mysqli $connection = NULL;
    private static $database = NULL;

    private function __construct()
    {
        $this->createConnection();
    }

    private function createConnection():void
    {
        $host = Config::get('host');
        $port = Config::get('port');
        $db   = Config::get('db');
        $user = Config::get('user');
        $pass = Config::get('pass');

        $this->connection = mysqli_connect(
            $host,
            $user,
            $pass,
            $db,
            $port
        );
        if (!$this->connection) {
            exit("Connection failed: " . mysqli_connect_error());
        }
    }
    static function getInstance():DatabaseConnector {
        if (NULL == self::$database) {
            self::$database = new DatabaseConnector();
        }
        return self::$database;
    }

    public function getConnection(): ?mysqli {
        return $this->connection;
    }
}