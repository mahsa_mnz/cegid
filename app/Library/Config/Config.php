<?php
namespace App\Library\Config;

class Config
{
    private static $config;

    /**
     * This static function get data for key name in config directory in root file
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public static function get($key, $default = null)
    {
        if (is_null(self::$config)) {
            self::$config = require_once(__DIR__ . '/../../../config/app.php');
        }

        return !empty(self::$config[$key]) ? self::$config[$key] : $default;
    }
}