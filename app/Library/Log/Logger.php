<?php

namespace App\Library\Log;

use App\Library\Config\Config;
use Monolog\ErrorHandler;
use Monolog\Handler\StreamHandler;

class Logger extends \Monolog\Logger
{
    private static array $loggers = [];

    /**
     * This private constructor couldn't be called out of this class
     * @param string $key
     * @param string|null $logLevel
     */
    private function __construct(string $key = "app", string $logLevel = null)
    {
        parent::__construct($key);
        $logPath = Config::get('log_path');

        $logFile = "{$logPath}/{$key}.log";

        if ($logLevel == null) {
            $logLevel = \Monolog\Logger::DEBUG;
        }

        $this->pushHandler(new StreamHandler($logFile, $logLevel));
    }

    /**
     * This function restricts the instantiation of a class to one "single" instance
     * this will be called instead of constructor
     * @param string $key
     * @param string|null $logLevel
     * @return Logger
     */
    public static function getInstance(string $key = "app", string $logLevel = null): Logger
    {
        if (empty(self::$loggers[$key])) {
            self::$loggers[$key] = new Logger($key, $logLevel);
        }

        return self::$loggers[$key];
    }

    /**
     * This function will save log for all requests and errors in directory path which is defined in config file
     */
    public static function enableSystemLogs()
    {
        $logPath = Config::get('log_path');

        // Error Log
        self::$loggers['error'] = self::getInstance('errors');
        self::$loggers['error']->pushHandler(new StreamHandler("{$logPath}/errors.log"));
        ErrorHandler::register(self::$loggers['error']);

        // Request Log
        $data = [
            $_SERVER,
            $_REQUEST,
            trim(file_get_contents("php://input"))
        ];
        self::$loggers['request'] = self::getInstance('request');
        self::$loggers['request']->pushHandler(new StreamHandler("{$logPath}/request.log"));
        self::$loggers['request']->info("REQUEST", $data);
    }
}