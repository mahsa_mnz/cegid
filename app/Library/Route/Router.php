<?php

namespace App\Library\Route;

use App\Exceptions\NotFoundException;
use JetBrains\PhpStorm\Pure;

class Router
{
    const GET = 'GET';
    const POST = 'POST';
    const PATCH = 'PATCH';
    const DELETE = 'DELETE';

    public static function parse($route, array $callbackArray, array $regex = null)
    {
        if (array_key_exists($_SERVER['REQUEST_METHOD'], $callbackArray))
            self::on($route, $callbackArray[$_SERVER['REQUEST_METHOD']], $regex);
        return null;
    }

    /**
     * This function will check the regex route and extract the parameters in query URL
     * then call the callback function with the Request class
     * @param $route
     * @param $callBack
     * @param array|null $regex
     */
    private static function on($route, $callBack, array $regex = null)
    {
        $mainRoute = strtok($_SERVER["REQUEST_URI"], '?');
        $mainRoute = (stripos($mainRoute, "/") !== 0) ? "/" . $mainRoute : $mainRoute;

        $routeRegex = str_replace('/', '\/', $route);
        if ($regex != null){
            foreach ($regex as $key => $value)
            {
                $routeRegex = str_replace("{".$key."}", $value, $routeRegex);
            }
        }
        $routeRegex = '/^' . ($routeRegex) . '$/';
        $is_match = preg_match($routeRegex, $mainRoute, $matches, PREG_OFFSET_CAPTURE);

        if ($is_match) {
            $params = self::getParams($route, $mainRoute);
            $callBack(new Request($params));
        }
    }

    /**
     * This function extract the parameters in query URL based on the query regex
     * @param $routeRegex
     * @param $route
     * @return array
     */
    #[Pure] private static function getParams ($routeRegex, $route): array
    {
        $response = array();
        $explodedRegex = explode("/", $routeRegex);
        $explodedRoute = explode("/", $route);

        foreach ($explodedRegex as $key => $value)
        {
            if (isStringWrappedWithBrace($value)){
                $response[getStringBetweenBrace($value)] = $explodedRoute[$key];
            }
        }

        return $response;
    }
}