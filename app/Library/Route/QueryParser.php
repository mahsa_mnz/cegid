<?php

namespace App\Library\Route;

class QueryParser
{
    const EQ = ":eq:";
    const GT = ":gt:";
    const GTE = ":gte:";
    const ST = ":st:";
    const STE = ":ste:";

    public static function parseQueryParams(array $params): array
    {
        $whereArray = array();

        foreach ($params as $key => $value)
        {
            if (str_starts_with($value, self::EQ))
            {
                $value = substr($value, strlen(self::EQ));
                $whereArray[] = "`$key` = '$value'";
            }
            elseif (str_starts_with($value, self::GT))
            {
                $value = substr($value, strlen(self::GT));
                $whereArray[] = "`$key` > $value";
            }
            elseif (str_starts_with($value, self::GTE))
            {
                $value = substr($value, strlen(self::GTE));
                $whereArray[] = "`$key` >= $value";
            }
            elseif (str_starts_with($value, self::ST))
            {
                $value = substr($value, strlen(self::ST));
                $whereArray[] = "`$key` < $value";
            }
            elseif (str_starts_with($value, self::STE))
            {
                $value = substr($value, strlen(self::STE));
                $whereArray[] = "`$key` <= $value";
            }
        }

        return $whereArray;
    }
}