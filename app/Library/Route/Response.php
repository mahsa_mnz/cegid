<?php

namespace App\Library\Route;

class Response
{
    const RESPONSE_OK = 200;
    const RESPONSE_CREATED = 201;
    const RESPONSE_NOT_FOUND = 404;
    const RESPONSE_BAD_REQUEST = 400;
    const RESPONSE_FORBIDDEN = 403;
    const RESPONSE_UNAUTHORIZED = 401;
    const RESPONSE_SERVER_ERROR = 500;


    private int $status = self::RESPONSE_OK;

    /**
     * @param int $code
     * @return Response
     */
    public function status(int $code): static
    {
        $this->status = $code;
        return $this;
    }

    public function toJSON(array $data = []): bool|string
    {
        http_response_code($this->status);
        header('Content-Type: application/json');
        return json_encode($data);
    }
}