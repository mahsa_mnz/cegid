<?php

namespace App\Library\Route;

class Request
{
    public array $params;
    public string $reqMethod;
    public string $contentType;

    public function __construct($params = [])
    {
        $this->params = $params;
        $this->reqMethod = trim($_SERVER['REQUEST_METHOD']);
        $this->contentType = !empty($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
    }

    public function getQueryStringParams(): array
    {
        parse_str($_SERVER['QUERY_STRING'], $queryParams);

        return $queryParams;
    }

    public function getBody(): array
    {
        $body = [];
        if (in_array($this->reqMethod, [Router::POST, Router::PATCH])) {
            if ($this->contentType == 'application/json')
            {
                // Receive the RAW post data.
                $content = trim(file_get_contents("php://input"));
                $body = json_decode($content, true);
            }
            else
            {
                foreach ($_POST as $key => $value) {
                    $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
                }
            }
        }
        return $body;
    }
}