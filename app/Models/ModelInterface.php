<?php

namespace App\Models;

interface ModelInterface
{
    public function getTableName(): string;
    public function getIdName(): string;
}