<?php

namespace App\Models;

class Hotelier implements ModelInterface
{
    private string $tableName = 'hoteliers';
    private string $idName = 'id';

    public function getTableName(): string
    {
        return $this->tableName;
    }

    public function getIdName(): string
    {
        return $this->idName;
    }

    public function calculateReputationBadge(int $reputation): string
    {
        if ($reputation <= 500)
            return "red";
        elseif ($reputation <= 799)
            return "yellow";
        else
            return "green";
    }
}
