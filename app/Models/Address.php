<?php

namespace App\Models;

class Address implements ModelInterface
{
    private string $tableName = 'addresses';
    private string $idName = 'id';

    public function getTableName(): string
    {
        return $this->tableName;
    }

    public function getIdName(): string
    {
        return $this->idName;
    }
}