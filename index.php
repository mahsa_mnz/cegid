<?php

use App\Controllers\Hotelier\HotelierController;
use App\Library\Route\Router;

require __DIR__ . '/bootstrap/bootstrap.php';

Router::parse("/hoteliers", [
    Router::GET => [new HotelierController(), 'index'],
    Router::POST => [new HotelierController(), 'create']
]);

Router::parse("/hoteliers/{hotelier_id}", [
    Router::GET => [new HotelierController(), 'show'],
    Router::PATCH => [new HotelierController(), 'update'],
    Router::DELETE => [new HotelierController(), 'delete']
], [
    'hotelier_id' => '[1-9][0-9]*',
]);

Router::parse("/hoteliers/{hotelier_id}/book",[
    Router::POST => [new HotelierController(), 'book']
], [
    'hotelier_id' => '[1-9][0-9]*',
]);